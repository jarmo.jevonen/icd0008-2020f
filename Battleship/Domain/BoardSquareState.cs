namespace Domain
{
    public class BoardSquareState
    {
        // value from GameBoat.GameBoatId
        public int? GameBoatId { get; set; }

        // 0 - no bomb yet, 1....n order of bombs
        public int Bomb { get; set; }
    }
}