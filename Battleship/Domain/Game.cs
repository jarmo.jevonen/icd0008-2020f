using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Enums;

namespace Domain
{
    public class Game
    {
        public int GameId { get; set; }

        public string Description { get; set; } = DateTime.Now.ToLongDateString();

        [DisplayName("game over")]public bool isGameOver { get; set; }
        
        // Help out EF to set players to right fields. find Model builder from slides.
        public int PlayerAId { get; set; }
        [DisplayName("Player A")]public Player PlayerA { get; set; } = default!;
        
        
        public int PlayerBId { get; set; }
        [DisplayName("Player B")]public Player PlayerB { get; set; } = default!;
        
        [DisplayName("Width")] public int BoardWidth { get; set; }
        [DisplayName("Height")]public int BoardHeight { get; set; }
        
        [DisplayName("Boats can touch")]public EBoatsCanTouch EBoatsCanTouch { get; set; }

        [DisplayName("Can move after hit")]public bool CanSamePlayerMoveAfterHit { get; set; }

        [DisplayName("Is player A turn")]public bool IsPlayerATurn { get; set; }
    }
}