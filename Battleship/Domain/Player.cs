using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Domain.Enums;

namespace Domain
{
    public class Player
    {
        public int PlayerId { get; set; }
        [MaxLength(128)]
        public string Name { get; set; } = default!;

        public EPlayerType EPlayerType { get; set; }


        public int? GameAId { get; set; }
        public Game? GameA { get; set; }
        
        public int? GameBId { get; set; }
        public Game? GameB { get; set; }

        public ICollection<GameBoat> GameBoats { get; set; } = default!;
        
        public ICollection<PlayerBoardState> PlayerBoardStates { get; set; } = default!;

        public string PlayerBoardStatesLastBoardStateJson => PlayerBoardStates?.Last()?.BoardState ?? "";
    }
}