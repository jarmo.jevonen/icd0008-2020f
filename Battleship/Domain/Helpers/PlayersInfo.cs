﻿using Domain.Enums;

namespace Domain.Helpers
{
    public class PlayersInfo
    {
        public string PlayerAName { get; set; } = default!;
        public string PlayerBName { get; set; } = default!;
        public EPlayerType PlayerAType { get; set; }
        public EPlayerType PlayerBType { get; set; }
    }
}