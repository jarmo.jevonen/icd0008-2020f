using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Boat
    {
        public int BoatId { get; set; }
        
        public ICollection<GameOptionBoat>? GameOptionBoats { get; set; }
        
        [Range(1, 20)]
        public int Size { get; set; }
        
        [MaxLength(25)]
        public string Name { get; set; } = null!;
    }
}       