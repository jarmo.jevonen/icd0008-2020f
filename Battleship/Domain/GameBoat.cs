using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class GameBoat
    {
        public int GameBoatId { get; set; }
        
        
        [Range(1, int.MaxValue)]
        public int Size { get; set; }
        
        [MaxLength(25)]
        public string Name { get; set; } = default!;

        public bool IsSunken { get; set; }


        public int PlayerId { get; set; }
        public Player Player { get; set; } = default!;
        
    }
}