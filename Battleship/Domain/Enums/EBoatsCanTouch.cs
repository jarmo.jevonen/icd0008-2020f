namespace Domain.Enums
{
    public enum EBoatsCanTouch
    {
        No = 0,
        Corner = 1,
        Yes = 2,
    }
    
}