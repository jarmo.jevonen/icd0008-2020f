﻿namespace GameBrain
{
    public class GameSettings
    {
        public int GameBoardHeight { get; set; }
        public int GameBoardWidth { get; set; }

        public GameSettings()
        {
            
        }
         
        public GameSettings(int gameBoardHeight, int gameBoardWidth)
        {
            GameBoardHeight = gameBoardHeight;
            GameBoardWidth = gameBoardWidth;
        }
    }
}
