﻿namespace GameBrain
{
    public class GameState
    {
        public int NextMoveByPlayer { get; set; }
        public CellState[][] BoardPlayer1 { get; set; } = null!;
        public CellState[][] BoardPlayer2 { get; set; } = null!;
        public int Width { get; set; }
        public int Height { get; set; }
    }
}