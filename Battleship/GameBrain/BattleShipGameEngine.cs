﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml.Schema;
using DAL;
using Domain;
using Domain.Enums;
using Domain.Helpers;


namespace GameBrain
{
    public class BattleShipGameEngine
    {
        private BoardState PlayerABoardState { get; set; } = default!;
        private BoardState PlayerBBoardState { get; set; } = default!;
        private IGameController GameController { get; set; }
        private Game Game { get; set; } = default!;

        private SaveGameHandler _saveGameHandler;
        private GameOptionsHandler _gameOptionsHandler;
        private GameDataHandler _gameDataHandler;
        

        public BattleShipGameEngine(AppDbContext context, IGameController gameController)
        {
            _saveGameHandler = new SaveGameHandler(context);
            _gameOptionsHandler = new GameOptionsHandler(context);
            _gameDataHandler = new GameDataHandler(context);
            GameController = gameController;
        }

        public Game LoadGameById(int gameId)
        {
            Game = _saveGameHandler.LoadGameById(gameId);
            PlayerABoardState = JsonSerializer.Deserialize<BoardState>(Game.PlayerA.PlayerBoardStates.Last().BoardState) ?? throw new InvalidOperationException("Could not deserialize player A board json (no boards)");
            PlayerBBoardState = JsonSerializer.Deserialize<BoardState>(Game.PlayerB.PlayerBoardStates.Last().BoardState) ?? throw new InvalidOperationException("Could not deserialize player B board json (no boards)");
            return Game;
        }

        public int CreateNewGame(int gameOptionId, PlayersInfo playersInfo)
        {
            var gameOption = _gameOptionsHandler.LoadGameOptionById(gameOptionId);
            return InitializeGame(gameOption, playersInfo);
        }

        public int SaveGameOption(GameOption gameOption)
        {
            SetGameOptionBoats(gameOption);
            return _gameOptionsHandler.SaveGameOption(gameOption);
        }

        private void SetGameOptionBoats(GameOption gameOption)
        {
            
            var area = gameOption.BoardHeight * gameOption.BoardWidth;
            var boatsDictionary = new Dictionary<Boat, int>();
            var tilesToCoverWithBoats = area * C.DefaultGameOptionBoatToBoardRatio / 100;
            var boats = _gameDataHandler
                .GetAllBoats()
                .Where(b => b.Size <= gameOption.BoardHeight && b.Size <= gameOption.BoardWidth)
                .OrderBy(b => b.Size)
                .ToList();
            var i = 0;
            while (boatsDictionary.Sum((kvp) => kvp.Key.Size * kvp.Value) < tilesToCoverWithBoats)
            {
                boatsDictionary[boats[i % boats.Count]] = boatsDictionary.GetValueOrDefault(boats[i % boats.Count], 0) + 1;
                i++;
            }

            ICollection<GameOptionBoat> gameOptionBoats = boatsDictionary
                .Select(keyValuePair => new GameOptionBoat
                {
                    Boat = keyValuePair.Key,
                    Quantity = keyValuePair.Value
                })
                .ToList();
            gameOption.GameOptionBoats = gameOptionBoats;
        }

        public void PlaceShipsRandomly(int gameOptionId)
        {
            var gameOption = _gameDataHandler.GetGameOption(gameOptionId).Result;
            var random = new Random();
            var gameOptionBoats = gameOption.GameOptionBoats
                .OrderBy(gob => gob.Boat.Size)
                .Reverse()
                .ToList();
            var boatsToBePlaced = new List<GameOptionBoat>();
            foreach (var gameOptionBoat in gameOptionBoats)
            {
                for (int j = 0; j < gameOptionBoat.Quantity; j++)
                {
                    boatsToBePlaced.Add(gameOptionBoat);
                }
            }
            var i = 0;
            while (i != gameOptionBoats.Sum(gob => gob.Quantity))
            {
                var y = random.Next(Game.BoardHeight);
                var x = random.Next(Game.BoardWidth);
                var isVertical = random.Next(2) == 1;
                if (PlaceShipOnBoard(y, x, isVertical, boatsToBePlaced[i].GameOptionBoatId))
                {
                    i++;
                }
            }
        }
        
        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) PlayerSelected(int y, int x)
        {
            if (!Game.isGameOver)
            {
                var boardSquareState = Game.IsPlayerATurn ? PlayerBBoardState.Board[y][x] : PlayerABoardState.Board[y][x];
                boardSquareState.Bomb = 1;
                if (boardSquareState.GameBoatId != null)
                {
                    var gameBoatId = boardSquareState.GameBoatId;
                    if (IsOpponentShipByIdSunken(gameBoatId!.Value)) Game.isGameOver = CheckCurrentPlayerWon();
                }
                if (!Game.isGameOver)
                {
                    Game.IsPlayerATurn = !Game.IsPlayerATurn;
                }
                AddPlayersBoardStatesToGame();
                _saveGameHandler.UpdateGame(Game);
            }
            return GetBoards();
        }

        private bool IsOpponentShipByIdSunken(int gameBoatId)
        {
            var opponent = Game.IsPlayerATurn ? Game.PlayerB : Game.PlayerA;
            var gameBoat = opponent.GameBoats.FirstOrDefault(gb => gb.GameBoatId == gameBoatId);
            var opponentBoardSquareState = opponent == Game.PlayerA ? PlayerABoardState.Board : PlayerBBoardState.Board;
            var sunkenCount = opponentBoardSquareState
                .Sum(boardRow => boardRow
                    .Count(boardSquareState => boardSquareState.GameBoatId == gameBoatId && boardSquareState.Bomb != 0));
            if (sunkenCount == gameBoat!.Size)
            {
                gameBoat.IsSunken = true;
                return true;
            }
            return false;
        }

        private bool CheckCurrentPlayerWon()    
        {
            var opponent = Game.IsPlayerATurn ? Game.PlayerB : Game.PlayerA;
            return opponent.GameBoats.All(opponentShip => opponentShip.IsSunken);
        }

        private void AddPlayersBoardStatesToGame()
        {
            var playerABoardState = new PlayerBoardState
            {
                BoardState = JsonSerializer.Serialize(PlayerABoardState),
                CreatedAt = DateTime.Now,
                Player = Game.PlayerA
            };
            var playerBBoardState = new PlayerBoardState
            {
                BoardState = JsonSerializer.Serialize(PlayerBBoardState),
                CreatedAt = DateTime.Now,
                Player = Game.PlayerB
            };
            if (Game.PlayerA.PlayerBoardStates == null)
            {
                Game.PlayerA.PlayerBoardStates = new List<PlayerBoardState>();
                Game.PlayerB.PlayerBoardStates = new List<PlayerBoardState>();
            }
            Game.PlayerA.PlayerBoardStates.Add(playerABoardState);
            Game.PlayerB.PlayerBoardStates.Add(playerBBoardState);
        }


        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) GetBoards()
        {
            var player1Board = new BoardState
            {
                Board = new BoardSquareState[Game.BoardHeight][]
            };
            FillBoardArrays(player1Board);
            var player2Board = new BoardState
            {
                Board = new BoardSquareState[Game.BoardHeight][]
            };
            FillBoardArrays(player2Board);
            Array.Copy(PlayerABoardState.Board, player1Board.Board, PlayerABoardState.Board.Length);
            Array.Copy(PlayerBBoardState.Board, player2Board.Board, PlayerBBoardState.Board.Length);
            if (Game.IsPlayerATurn)
            {
                return (player1Board.Board, player2Board.Board);
            }
            return (player2Board.Board, player1Board.Board);
        }

        private void FillBoardArrays(BoardState boardState)
        {
            var board = boardState.Board;
            for (int i = 0; i < Game.BoardHeight; i++)
            {
                board[i] = new BoardSquareState[Game.BoardWidth];
                for (int j = 0; j < Game.BoardWidth; j++)
                {
                    board[i][j] = new BoardSquareState();
                }
            }
        }


        private int InitializeGame(GameOption gameOption, PlayersInfo playersInfo)
        {
            var playerA = new Player
            {
                Name = playersInfo.PlayerAName,
                EPlayerType = playersInfo.PlayerAType,
            };
            var playerB = new Player{
                Name = playersInfo.PlayerBName,
                EPlayerType = playersInfo.PlayerBType,
            };
            Game = new Game
            {
                BoardHeight = gameOption.BoardHeight,
                BoardWidth = gameOption.BoardWidth,
                EBoatsCanTouch = gameOption.EBoatsCanTouch,
                CanSamePlayerMoveAfterHit = gameOption.CanSamePlayerMoveAfterHit,
                IsPlayerATurn = true,
                PlayerA = playerA,
                PlayerB = playerB
            };
            PlayerABoardState = new BoardState
            {
                Board = new BoardSquareState[Game.BoardHeight][]
            };
            FillBoardArrays(PlayerABoardState);
            PlayerBBoardState = new BoardState
            {
                Board = new BoardSquareState[Game.BoardHeight][]
            };
            FillBoardArrays(PlayerBBoardState);
            AddPlayersBoardStatesToGame();
            _saveGameHandler.AddGame(Game);
            return Game.GameId;
        }

        public List<Tuple<int, int>> FindAdjacentTilesForPlacement(int y, int x, EBoatsCanTouch boatsCanTouch)
        {
            var canGoDown = false;
            var canGoUp = false;
            var adjacentTiles = new List<Tuple<int, int>>();
            adjacentTiles.Add(new Tuple<int, int>(y, x));
            if (boatsCanTouch == EBoatsCanTouch.Yes)
            {
                return adjacentTiles;
            }
            if (y > 0)
            {
                adjacentTiles.Add(new Tuple<int, int>(y - 1, x));
                canGoUp = true;
            }
            if (y < Game.BoardHeight - 1)
            {
                adjacentTiles.Add(new Tuple<int, int>(y + 1, x));
                canGoDown = true;
            }
            if (x > 0)
            {
                adjacentTiles.Add(new Tuple<int, int>(y, x - 1));
                if (boatsCanTouch == EBoatsCanTouch.No)
                {
                    if (canGoUp)
                    {
                        adjacentTiles.Add(new Tuple<int, int>(y - 1, x - 1));
                    }
                    if (canGoDown)
                    {
                        adjacentTiles.Add(new Tuple<int, int>(y + 1, x - 1));
                    }
                }
            }
            if (x < Game.BoardHeight - 1)
            {
                adjacentTiles.Add(new Tuple<int, int>(y, x + 1));
                if (boatsCanTouch == EBoatsCanTouch.No)
                {
                    if (canGoUp)
                    {
                        adjacentTiles.Add(new Tuple<int, int>(y - 1, x + 1));
                    }
                    if (canGoDown)
                    {
                        adjacentTiles.Add(new Tuple<int, int>(y + 1, x + 1));
                    }
                }
            }
            return adjacentTiles;
        }

        private bool TryAndPlaceShipVertically(int y, int x, Player player, GameBoat gameBoat)
        {
            var boatLenght = gameBoat.Size;
            var playerBoard = player.GameA != null ? PlayerABoardState.Board : PlayerBBoardState.Board;
            var canPlaceShip = true;
            if (y + boatLenght <= Game.BoardHeight)
            {
                for (var i = 0; i < boatLenght; i++)
                {
                    if (FindAdjacentTilesForPlacement(y + i, x, Game.EBoatsCanTouch)
                        .Exists(b => playerBoard[b.Item1][b.Item2].GameBoatId != null))
                    {
                        canPlaceShip = false;
                    }
                }
                if (canPlaceShip)
                {
                    gameBoat.Player = player;
                    _gameDataHandler.SaveGameBoat(gameBoat);
                    player.GameBoats.Add(gameBoat);
                    for (var i = 0; i < boatLenght; i++)
                    {
                        playerBoard[y + i][x].GameBoatId = gameBoat.GameBoatId;
                    }
                    AddPlayersBoardStatesToGame();
                    _saveGameHandler.UpdateGame(Game);
                    return true;
                }
            }
            return false;
        }


        private bool TryAndPlaceShipHorizontally(int y, int x, Player player, GameBoat gameBoat)
        {
            var boatLenght = gameBoat.Size;
            var playerBoard = player.GameA != null ? PlayerABoardState.Board : PlayerBBoardState.Board;
            var canPlaceShip = true;
            if (x + boatLenght <= Game.BoardWidth)
            {
                for (var i = 0; i < boatLenght; i++)
                {
                    if (FindAdjacentTilesForPlacement(y, x + i, Game.EBoatsCanTouch)
                        .Exists(b => playerBoard[b.Item1][b.Item2].GameBoatId != null))
                    {
                        canPlaceShip = false;
                    }
                }
                if (canPlaceShip)
                {
                    gameBoat.Player = player;
                    _gameDataHandler.SaveGameBoat(gameBoat);
                    player.GameBoats.Add(gameBoat);
                    for (var i = 0; i < boatLenght; i++)
                    {
                        playerBoard[y][x + i].GameBoatId = gameBoat.GameBoatId;
                    }
                    AddPlayersBoardStatesToGame();
                    _saveGameHandler.UpdateGame(Game);
                    return true;
                }
            }
            return false;
        }

        public bool PlaceShipOnBoard(int y, int x, bool isVertical, int gameOptionBoatId)
        {
            var isPlaced = false;
            var gameOptionBoat = _gameDataHandler.GetGameOptionBoatById(gameOptionBoatId);
            var playerABoatsCount = Game.PlayerA.GameBoats.Count();
            if (gameOptionBoat == null) return isPlaced;
            var boatsRequiredCount = gameOptionBoat.GameOption.GameOptionBoats.Sum(gob => gob.Quantity);
            var player = playerABoatsCount < boatsRequiredCount ? Game.PlayerA : Game.PlayerB;
            var gameBoat = new GameBoat()
            {
                Size = gameOptionBoat.Boat.Size,
                IsSunken = false,
                Name = gameOptionBoat.Boat.Name
            };
            if (isVertical)
            {
                isPlaced = TryAndPlaceShipVertically(y, x, player, gameBoat);
                return isPlaced;
            }

            isPlaced = TryAndPlaceShipHorizontally(y, x, player, gameBoat);
            return isPlaced;
        }
    }
}