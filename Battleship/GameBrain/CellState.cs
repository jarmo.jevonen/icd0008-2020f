namespace GameBrain
{
    public enum CellState
    {
        EMPTY = 0,
        BOMB_MISS = 1,
        BOMB_HIT = 2,
        BOAT = 3
    }
}