﻿using Domain.Enums;

namespace GameBrain
{
    public class C
    {
        public static readonly string GameSettingsFilePath = "gameSettings.txt";
        
        
        public const int DefaultGameOptionBoardHeight = 10;
        public const int DefaultGameOptionBoardWidth = 10;
        public const int DefaultGameOptionBoatToBoardRatio = 15;
        public const string DefaultGameOptionName = "default";
        public const EBoatsCanTouch DefaultGameOptionBoatsCanTouch = EBoatsCanTouch.Corner;
        public const bool DefaultGameOptionCanSamePlayerMoveAfterHit = true;
        
        
        public const int MinBoardWidth = 8;
        public const int MaxBoardWidth = 99;

        public const int MinBoardHeight = 8;
        public const int MaxBoardHeight = 26;

        
        
        public static readonly int BoardHeight = 5;
        public static readonly int BoardWidth = 5;
        public static readonly int CellEmpty = 0;
        public static readonly int CellMiss = 1;
        public static readonly int CellHit = 2;
        public static readonly int CellBoat = 3;
    }
}