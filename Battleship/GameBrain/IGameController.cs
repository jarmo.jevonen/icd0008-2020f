﻿using System.Threading.Tasks;
using Domain;
using Domain.Helpers;

namespace GameBrain
{
    public interface IGameController
    {
        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) PlayerSelected(string input);

        public void DisplayUiChange(BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard);

        public Game LoadGame(int gameId);

        public int CreateNewGame(int gameOptionId, PlayersInfo playersInfo);

        public int SaveGameOption(GameOption gameOption);
        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) GetBoards();

        public bool PlaceShip(int gameOptionBoatId, int y, int x, bool isVertical);

        public void PlaceShipsRandomly(int gameOptionId);
    }
}