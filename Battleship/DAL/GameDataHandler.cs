﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class GameDataHandler
    {
        private AppDbContext _context { get; set; }
        
        public GameDataHandler(AppDbContext appDbContext)
        {
            _context = appDbContext;
        }

        public ICollection<Boat> GetAllBoats()
        {
            return _context.Boats.ToList();
        }

        public GameOptionBoat? GetGameOptionBoatById(int gameOptionBoatId)
        {
            return _context.GameOptionBoats
                .Include(gob => gob.Boat)
                .Include(gob => gob.GameOption)
                    .ThenInclude(go => go.GameOptionBoats)
                .FirstOrDefault(gob => gob.GameOptionBoatId == gameOptionBoatId);
        }

        public void SaveGameBoat(GameBoat gameBoat)
        {
            _context.GameBoats.Add(gameBoat);
            _context.SaveChanges();
        }

        public async Task<IEnumerable<GameOptionBoat>> GetGameOptionBoats(int gameOptionId)
        {
            return await _context.GameOptionBoats
                .Include(gob => gob.Boat)
                .Where(x => x.GameOptionId == gameOptionId).ToListAsync();
        }

        public async Task<Game> GetGame(int gameId)
        {
            return await _context.Games
                .Include(g => g.PlayerA)
                    .ThenInclude(player => player.PlayerBoardStates)
                .Include(g => g.PlayerB)
                    .ThenInclude(player => player.PlayerBoardStates)
                .Include(g => g.PlayerA)
                    .ThenInclude(player => player.GameBoats)
                .Include(g => g.PlayerB)
                    .ThenInclude(player => player.GameBoats)
                .FirstOrDefaultAsync(x => x.GameId == gameId);
        }

        public async Task<GameOption> GetGameOption(int gameOptionId)
        {
            return await _context.GameOptions
                .Include(go => go.GameOptionBoats)
                .ThenInclude(gob => gob.Boat)
                .FirstOrDefaultAsync(go => go.GameOptionId == gameOptionId);
        }
    }
}