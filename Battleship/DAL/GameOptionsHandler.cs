﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using Domain;

namespace DAL
{
    public class GameOptionsHandler
    {
        private AppDbContext _context;
        
        public GameOptionsHandler(AppDbContext context)
        {
            _context = context;
        }

        public int SaveGameOption(GameOption gameOption)
        {
            _context.GameOptions.Add(gameOption);
            _context.SaveChanges();
            return gameOption.GameOptionId;
        }

        public GameOption LoadGameOptionById(int id)
        {
            return _context.GameOptions.Find(id);
        }

        public List<GameOption> GetAllGameOptions()
        {
            return _context.GameOptions.ToList();
        }
    }
}