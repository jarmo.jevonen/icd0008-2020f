﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace GameBrain
{
    
    public class SaveGameHandler
    {
        private AppDbContext _context { get; set; }
        public SaveGameHandler(AppDbContext appDbContext)
        {
            _context = appDbContext;
        }

        public void AddGame(Game game)
        {
            _context.Games.Add(game);
            _context.SaveChanges();
            game.PlayerA.GameAId = game.GameId;
            game.PlayerB.GameBId = game.GameId;
            _context.SaveChanges();
        }

        public void UpdateGame(Game game)
        {
            _context.Games.Update(game);
            _context.SaveChanges();
        }

        public List<Game> GetAllGames()
        {
            return _context.Games.ToList();
        }

        public Game LoadGameById(int id)
        {
            return _context.Games
                .Include(game => game.PlayerA)
                    .ThenInclude(player => player.PlayerBoardStates)
                .Include(game => game.PlayerB)
                    .ThenInclude(player => player.PlayerBoardStates)
                .Include(game => game.PlayerA)
                    .ThenInclude(player => player.GameBoats)
                .Include(game => game.PlayerB)
                    .ThenInclude(player => player.GameBoats)
                .FirstOrDefault(game => game.GameId == id);
        }
    }
}