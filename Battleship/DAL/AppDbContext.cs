﻿using System;
using System.Linq;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<GameOption> GameOptions { get; set; } = null!;
        public DbSet<Game> Games { get; set; } = null!;
        public DbSet<Player> Players { get; set; } = null!;

        public DbSet<GameOptionBoat> GameOptionBoats { get; set; } = null!;

        public DbSet<Boat> Boats { get; set; } = null!;

        public DbSet<GameBoat> GameBoats { get; set; } = null!;
        
        
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder
                .Entity<GameOptionBoat>()
                .HasIndex(i => new
                {
                    i.BoatId,
                    i.GameOptionId
                })
                .IsUnique();

            modelBuilder
                .Entity<Player>()
                .HasOne<Game>()
                .WithOne(x => x.PlayerB)
                .HasForeignKey<Game>(x => x.PlayerBId);

            modelBuilder
                .Entity<Player>()
                .HasOne<Game>()
                .WithOne(x => x.PlayerA)
                .HasForeignKey<Game>(x => x.PlayerAId);

            modelBuilder
                .Entity<Game>()
                .HasOne<Player>()
                .WithOne(x => x.GameA!)
                .HasForeignKey<Player>(x => x.GameAId);
                

            modelBuilder
                .Entity<Game>()
                .HasOne<Player>()
                .WithOne(x => x.GameB!)
                .HasForeignKey<Player>(x => x.GameBId);

            foreach (var relationship in modelBuilder.Model
                .GetEntityTypes()
                .Where(e => !e.IsOwned())
                .SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}