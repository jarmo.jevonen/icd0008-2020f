﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MenuSystem
{
    public enum MenuLevel
    {
        Level0,
        Level1,
        Level2Plus
    }
    
    public class Menu
    {

        private readonly string[] _reservedActions = {"x", "m", "r"};
        private Dictionary<string, MenuItem> MenuItems { get; set; } = new Dictionary<string, MenuItem>();
        private readonly MenuLevel _menuLevel;

        public Menu(MenuLevel level)
        {
            _menuLevel = level;
        }
        
        public void AddMenuItem(MenuItem item)
        {
            if (item.CommandKey == "")
            {
                throw new ArgumentException("UserChoice cannot be empty");
            }
            MenuItems.Add(item.CommandKey, item);
        }
        

        public string RunMenu() //needs to be of type Func<string>
        {
            Console.CursorVisible = false;
            int menuItemIndex = 0;
            Console.Clear();
            string userChoice = "";
            // if menu does not have exit options add them
            if (!MenuItems.ContainsKey("x"))
            {
                switch (_menuLevel)
                {
                    case MenuLevel.Level0:
                        AddMenuItem(new MenuItem("Exit", "x", () => "x"));
                        break;
                    case MenuLevel.Level1:
                        AddMenuItem(new MenuItem("return to Main", "m", () => "m"));
                        AddMenuItem(new MenuItem("Exit", "x", () => "x"));
                        break;
                    case MenuLevel.Level2Plus:
                        AddMenuItem(new MenuItem("Return to previous", "r", () => ""));
                        AddMenuItem(new MenuItem("return to Main", "m", () => "m"));
                        AddMenuItem(new MenuItem("Exit", "x", () => "x"));
                        break;
                    default:
                        throw new Exception("Unknown menu depth!");
                }
            }
            
            do
            {
                // prints out menu options
                do
                {
                    for (int i = 0; i < MenuItems.Values.Count; i++)
                    {
                        var menuitem = MenuItems.ElementAt(i);
                        if (menuItemIndex % MenuItems.Count == i)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkGreen;
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine($"{menuitem.Value.CommandKey}) {menuitem.Value.Label}");
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.WriteLine($"{menuitem.Value.CommandKey}) {menuitem.Value.Label}");
                        }
                    }
                    ConsoleKeyInfo consoleKey = Console.ReadKey(true);

                    if (consoleKey.Key == ConsoleKey.UpArrow && menuItemIndex != 0)
                    {
                        menuItemIndex--;
                    }
                    else if (consoleKey.Key == ConsoleKey.DownArrow)
                    {
                        menuItemIndex++;
                    }
                    else if (consoleKey.Key == ConsoleKey.Enter)
                    {
                        userChoice = MenuItems.ElementAt(menuItemIndex % MenuItems.Values.Count).Value.CommandKey;
                    }
                    else if (MenuItems.ContainsKey(consoleKey.KeyChar.ToString()))
                    {
                        userChoice = consoleKey.KeyChar.ToString();
                    }

                    Console.Clear();
                } while (userChoice == "");
                // Check if user is not exiting
                if (!_reservedActions.Contains(userChoice))
                {
                    // find users command and execute it
                    if (MenuItems.TryGetValue(userChoice, out var userMenuItem))
                    {
                        userChoice = userMenuItem.MethodToExecute();
                        // if user wants to return then only return to previous menu
                        if (userChoice == "r")
                        {
                            userChoice = "";
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("I don't have this option! '" + userChoice + "'");
                    }
                }
                // check where to exit
                if (userChoice == "x")
                {
                    if (_menuLevel == MenuLevel.Level0)
                    {
                        Console.WriteLine("Closing down");
                    }
                    break;
                }
                if (_menuLevel != MenuLevel.Level0 && userChoice == "m")
                {
                    break;
                }
                if (_menuLevel == MenuLevel.Level2Plus && userChoice == "r")
                {
                    break;
                }
            } while (true);

            // return user choice to previous menu
            return userChoice;
        }
    }
} 