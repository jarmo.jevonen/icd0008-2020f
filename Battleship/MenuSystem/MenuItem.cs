﻿using System;

namespace MenuSystem
{
    public class MenuItem
    {
        
        public virtual string Label { get; set; }
        public virtual string CommandKey { get; set; }
        public virtual Func<string> MethodToExecute { get; set; }
        
        public MenuItem(string label, string commandKey, Func<string> methodToExecute)
        {
            Label = label.Trim();
            CommandKey = Validate(commandKey, 1, 3, false);
            MethodToExecute = methodToExecute;
        }

        private string Validate(string value, int minLength, int maxLength, bool toUpper)
        {
            value = value.Trim();
            if (toUpper)
            {
                value = value.ToUpper();
            }
            if (minLength <= value.Length && maxLength >= value.Length)
            {
                return value;
            }
            else
            {
                throw new ArgumentException($"Command is not correct length (1)! Got {value.Length} characters. value:{value}.");
            }
        }
    }
}