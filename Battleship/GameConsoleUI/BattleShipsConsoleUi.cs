﻿using System;
using System.IO;
using System.Text;
using Domain;
using GameBrain;

namespace GameConsoleUI
{
    public static class BattleShipsConsoleUi
    {
        
        public static void DrawBoards(BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard)
        {
            Console.WriteLine("Your board");
            DrawBoardRow(playerBoard, true);
            Console.WriteLine();
            Console.WriteLine("Enemy board");
            DrawBoardRow(opponentBoard, false);
        }

        private static void CreateNumbersForColumns(BoardSquareState[][] board)
        {
            StringBuilder numbers = new StringBuilder();
            for (int col = 0; col < board[0].Length; col++)
            {
                numbers.Append($"{(col + 1).ToString().PadLeft(2, ' ')} |");
            }

            Console.WriteLine("|".PadLeft(4, ' ') + numbers.ToString());
        }

        private static void DrawBoardRow(BoardSquareState[][] board, bool shipsVisible)
        {
            for (int y = 0; y < board.Length; y++)
            {
                if (y == 0)
                {
                    CreateNumbersForColumns(board);
                }
                for (int x = 0; x < board[0].Length; x++)
                {
                    if (x == 0)
                    {
                        Console.Write($"{((char) ('a' + y)).ToString().PadLeft(3, ' ')}|");
                    }

                    var cellInt = 0;
                    
                    // boat is there and is hit
                    if (board[y][x].GameBoatId != null && board[y][x].Bomb != 0)
                    {
                        cellInt = 2;
                    }
                    // boat is there and is not hit
                    if (board[y][x].GameBoatId != null && board[y][x].Bomb == 0)
                    {
                        cellInt = shipsVisible ? 3 : 0;
                    }
                    // no boat and has bomb
                    if (board[y][x].GameBoatId == null && board[y][x].Bomb != 0)
                    {
                        cellInt = 1;
                    }
                    // no boat and no bomb
                    if (board[y][x].GameBoatId == null && board[y][x].Bomb == 0)
                    {
                        cellInt = 0;
                    }
                    Console.Write($" {CellString(cellInt)} |");
                }

                Console.WriteLine();
            }
        }

        private static string CellString(int cellState)
        {
            switch (cellState)
            {
                case 0 : return " "; // empty
                case 1 : return "O"; // miss
                case 2 : return "X"; // hit
                case 3 : return "H"; // boat
            }
            return "-";
        }
    }
}