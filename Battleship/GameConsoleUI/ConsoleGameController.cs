﻿using System;
using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Helpers;
using GameBrain;

namespace GameConsoleUI
{
    public class ConsoleGameController : IGameController
    {
        private BattleShipGameEngine GameEngine { get; set; }

        

        public ConsoleGameController(AppDbContext context)
        {
            GameEngine = new BattleShipGameEngine(context, this);
        }
        
        


        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) PlayerSelected(string input)
        {
            var inputs = input.Split(',');
            return GameEngine.PlayerSelected(int.Parse(inputs[0]), int.Parse(inputs[1]));
            
        }
        public void DisplayUiChange(BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard)
        {
            BattleShipsConsoleUi.DrawBoards(playerBoard, opponentBoard);
        }

        public Game LoadGame(int gameId)
        {
            return GameEngine.LoadGameById(gameId);
        }

        public int CreateNewGame(int gameOptionId, PlayersInfo playersInfo)
        {
            return GameEngine.CreateNewGame(gameOptionId, playersInfo);
        }

        public int SaveGameOption(GameOption gameOption)
        {
            return GameEngine.SaveGameOption(gameOption);
        }

        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) GetBoards()
        {
            return GameEngine.GetBoards();
        }

        public bool PlaceShip(int gameOptionBoatId, int y, int x, bool isVertical)
        {
            return GameEngine.PlaceShipOnBoard(y, x, isVertical, gameOptionBoatId);
        }

        public void PlaceShipsRandomly(int gameOptionId)
        {
            GameEngine.PlaceShipsRandomly(gameOptionId);
        }
    }
}