﻿using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Helpers;
using GameBrain;

namespace WebApp
{
    public class WebGameController : IGameController
    {
        private BattleShipGameEngine GameEngine { get; set; }

        public WebGameController(AppDbContext context)
        {
            GameEngine = new BattleShipGameEngine(context, this);
        }
        
        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) PlayerSelected(string input)
        {
            var yx = input.Split(",");
            return GameEngine.PlayerSelected(int.Parse(yx[0]), int.Parse(yx[1]));
        }

        public void DisplayUiChange(BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard)
        {
            throw new System.NotImplementedException();
        }

        public Game LoadGame(int gameId)
        {
            return GameEngine.LoadGameById(gameId);
        }

        public int CreateNewGame(int gameOptionId, PlayersInfo playersInfo)
        {
            return GameEngine.CreateNewGame(gameOptionId, playersInfo);
        }

        public int SaveGameOption(GameOption gameOption)
        {
            return GameEngine.SaveGameOption(gameOption);
        }

        public (BoardSquareState[][] playerBoard, BoardSquareState[][] opponentBoard) GetBoards()
        {
            throw new System.NotImplementedException();
        }

        public bool PlaceShip(int gameOptionBoatId, int y, int x, bool isVertical)
        {
            return GameEngine.PlaceShipOnBoard(y, x, isVertical, gameOptionBoatId);
        }

        public void PlaceShipsRandomly(int gameOptionId)
        {
            GameEngine.PlaceShipsRandomly(gameOptionId);
        }
    }
}