﻿using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace WebApp.Pages.GamePlay
{
    public class Index : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DAL.AppDbContext _context;
        public int BoardHeight { get; set; }
        public int BoardWidth { get; set; }

        public BoardState PlayerBoard { get; set; } = default!;
        public BoardState OpponentBoard { get; set; } = default!;

        [BindProperty] public string PlacementChoice { get; set; } = default!;
        
        [BindProperty(SupportsGet = true)] public int GameId { get; set; }
        
        public Index(DAL.AppDbContext context, ILogger<IndexModel> logger)
        {
            _logger = logger;
            _context = context;
        }

        public Game Game { get; set; } = default!;
        
        public async Task OnGetAsync(int gameId)
        {
            Game = await _context.Games
                .Where(x => x.GameId == gameId)
                .Include(g => g.PlayerA)
                    .ThenInclude(player => player.PlayerBoardStates)
                .Include(g => g.PlayerB)
                    .ThenInclude(player => player.PlayerBoardStates)
                .FirstOrDefaultAsync();
            BoardHeight = Game.BoardHeight;
            BoardWidth = Game.BoardWidth;
            
            var playerBoardJson = Game.IsPlayerATurn ? Game.PlayerA.PlayerBoardStates.Last().BoardState : Game.PlayerB.PlayerBoardStates.Last().BoardState;
            var opponentBoardJson = Game.IsPlayerATurn ? Game.PlayerB.PlayerBoardStates.Last().BoardState : Game.PlayerA.PlayerBoardStates.Last().BoardState;
            PlayerBoard = JsonSerializer.Deserialize<BoardState>(playerBoardJson)!;
            OpponentBoard = JsonSerializer.Deserialize<BoardState>(opponentBoardJson)!;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var webController = new WebGameController(_context);
            webController.LoadGame(GameId);
            webController.PlayerSelected(PlacementChoice);
            return RedirectToPage("./Index", new {GameId});
        }
    }
}