﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;
using Domain.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DAL.AppDbContext _context;
        
        public IndexModel(DAL.AppDbContext context, ILogger<IndexModel> logger)
        {
            _logger = logger;
            _context = context;
        }
        [BindProperty] [MinLength(1)] [MaxLength(64)] public string PlayerA { get; set; } = "Player A"; 
        
        [BindProperty] [MinLength(1)] [MaxLength(64)] public string PlayerB { get; set; } = "Player B";
        
        [BindProperty] public EPlayerType PlayerAType { get; set; }
        [BindProperty] public EPlayerType PlayerBType { get; set; }
        [BindProperty] [MinLength(1)] [MaxLength(64)] public string GameOptionsName { get; set; } = "Default game option";

        [BindProperty] [Range(4, 25)] public int BoardHeight { get; set; } = 10;
        [BindProperty] [Range(4, 25)] public int BoardWidth { get; set; } = 10;
        [BindProperty] public bool CanSamePlayerMoveAfterHit { get; set; }
        [BindProperty] public EBoatsCanTouch EBoatsCanTouch { get; set; }


        public IList<Game>? ExistingGames { get;set; }

        public async Task OnGetAsync()
        {
            ExistingGames = await _context.Games
                .Include(g => g.PlayerA)
                .Include(g => g.PlayerB).ToListAsync();
        }
        
        public async Task<IActionResult> OnPostAsync()
        {
            
            if (!ModelState.IsValid)
            {
                ExistingGames = await _context.Games
                    .Include(g => g.PlayerA)
                    .Include(g => g.PlayerB).ToListAsync();
                return Page();
            }
            var playerInfo = new PlayersInfo()
            {
                PlayerAName = PlayerA,
                PlayerBName = PlayerB,
                PlayerAType = PlayerAType,
                PlayerBType = PlayerBType
            };
            var gameOption = new GameOption()
            {
                Name = GameOptionsName,
                BoardHeight = BoardHeight,
                BoardWidth = BoardWidth,
                CanSamePlayerMoveAfterHit = CanSamePlayerMoveAfterHit,
                EBoatsCanTouch = EBoatsCanTouch,
            };
            var webController = new WebGameController(_context);
            var gameOptionId = webController.SaveGameOption(gameOption);
            var gameId = webController.CreateNewGame(gameOptionId, playerInfo);

            return RedirectToPage("./BoatPlacement/Index", new {gameId = gameId, gameOptionId = gameOptionId});
        }
    }
}