﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace WebApp.Pages.BoatPlacement
{
    public class Index : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DAL.AppDbContext _context;
        public int BoardHeight { get; set; }
        public int BoardWidth { get; set; }
        
        public IDictionary<GameOptionBoat, List<GameBoat>> BoatsDictionary { get; set; } = null!;

        public BoardState PlayerBoard { get; set; } = default!;

        [BindProperty(SupportsGet = true)] public bool IsVertical { get; set; }
        
        [BindProperty(SupportsGet = true)] public int GameId { get; set; } = default!;
        
        [BindProperty(SupportsGet = true)] public int GameOptionId { get; set; } = default!;

        [BindProperty] public int SelectedGameOptionBoatId { get; set; }
        
        [BindProperty] public string Coordinates { get; set; } = default!;
        
        [BindProperty] public bool IsRandomPlacement { get; set; }

        public string CurrentPlacerPlayerName { get; set; } = default!;
        
        
        public Index(DAL.AppDbContext context, ILogger<IndexModel> logger)
        {
            _logger = logger;
            _context = context;
        }

        public Game Game { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int gameId, int gameOptionId, bool isVertical)
        {
            IsVertical = isVertical;
            var gameDataHandler = new GameDataHandler(_context);
            Game = await gameDataHandler.GetGame(gameId);
            BoardHeight = Game.BoardHeight;
            BoardWidth = Game.BoardWidth;

            var gameOptionBoats = await gameDataHandler.GetGameOptionBoats(gameOptionId);
            var boardOwner = Game.PlayerA.GameBoats.Count != gameOptionBoats.Sum(gob => gob.Quantity)
                ? Game.PlayerA
                : Game.PlayerB;
            
            BoatsDictionary = gameOptionBoats
                .ToDictionary(x => x, gob => new List<GameBoat>(_context.GameBoats.Where(gb => gb.Name == gob.Boat.Name && gb.Size == gob.Boat.Size && gb.Player == boardOwner)));

            // var boardOwner = Game.PlayerA.GameBoats.Count != BoatsDictionary.Sum(entry => entry.Key.Quantity) ? Game.PlayerA : Game.PlayerB;
            var playerBoardJson = boardOwner.PlayerBoardStatesLastBoardStateJson;
            PlayerBoard = JsonSerializer.Deserialize<BoardState>(playerBoardJson)!;

            CurrentPlacerPlayerName = boardOwner.Name;
            if (Game.PlayerB.GameBoats.Count == BoatsDictionary.Sum(entry => entry.Key.Quantity))
            {
                return RedirectToPage("../GamePlay/Index", new {gameId = Game.GameId});
            }
            return null!;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var x = 0;
            var y = 0;
            var webController = new WebGameController(_context);
            var game = webController.LoadGame(GameId);
            if (IsRandomPlacement)
            {
                webController.PlaceShipsRandomly(GameOptionId);
            }
            else
            {
                var yx = Coordinates.Split(",");
                y = int.Parse(yx[0]);
                x = int.Parse(yx[1]);
                webController.PlaceShip(SelectedGameOptionBoatId, y, x, IsVertical);
            }
            return RedirectToPage("./Index", new {GameId, GameOptionId, IsVertical});
        }
    }
}