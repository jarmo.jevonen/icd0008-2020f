﻿using Domain;

namespace WebApp
{
    public static class CellHelper
    {
        public static string GetCellContent(BoardSquareState squareState, bool shipsVisible)
        {
            var result = "";
            if (squareState.GameBoatId == null && squareState.Bomb != 0)
            {
                result = "O";
            }
            // boat is there and is hit
            if (squareState.GameBoatId != null && squareState.Bomb != 0)
            {
                result = "X";
            }
            // boat is there and is not hit
            if (squareState.GameBoatId != null && squareState.Bomb == 0)
            {
                result = shipsVisible ? "H" : " ";
            }
            // no boat and has bomb
            if (squareState.GameBoatId == null && squareState.Bomb != 0)
            {
                result = "O";
            }
            // no boat and no bomb
            if (squareState.GameBoatId == null && squareState.Bomb == 0)
            {
                result = " ";
            }
            return result;
        }
    }
}