﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using Domain;
using Domain.Enums;
using Domain.Helpers;
using GameBrain;
using GameConsoleUI;
using MenuSystem;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp01
{
    internal static class Program
    {
        private static PlayersInfo _playersInfo = new PlayersInfo();

        private static int _currentGameOptionsId;

        private static IGameController _gameController = default!;

        private static int _gameToLoadId;
        
        private static GameOptionsHandler _gameOptionsHandler = default!;
        private static SaveGameHandler _saveGameHandler = default!;
        private static AppDbContext _appDbContext = default!;
        
        private static void Main()
        {
            var dbOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlServer(@"
                    Server=barrel.itcollege.ee,1533;
                    User Id=student;
                    Password=Student.Bad.password.0;
                    Database=jajevo_battleship;
                    MultipleActiveResultSets=true;
                    ").Options;
            using var db = new AppDbContext(dbOptions);
            db.Database.Migrate();
            _gameOptionsHandler = new GameOptionsHandler(db);
            _saveGameHandler = new SaveGameHandler(db);
            _appDbContext = db;
            
            _gameController = new ConsoleGameController(_appDbContext);
            
            
            var menu = new Menu(MenuLevel.Level0);
            menu.AddMenuItem(new MenuItem("Start game", "1", StartNewBattleShip));
            menu.AddMenuItem(new MenuItem("Load game", "l", ShowSavedGames));
            menu.AddMenuItem(new MenuItem("Game settings", "2", ShowGameOptions));
            menu.RunMenu();
        }

        private static string ShowSavedGames()
        {
            var gamesList = _saveGameHandler.GetAllGames();
            var savedGamesMenu = new Menu(MenuLevel.Level1);
            for (var i = 0; i < gamesList.Count; i++)
            {
                var game = gamesList[i];
                savedGamesMenu.AddMenuItem(new MenuItem(
                    $"Play '{game.Description}' game",
                    i.ToString(), () =>
                    {
                        _gameToLoadId = game.GameId;
                        PlayBattleShip(null);
                        return "";
                    }));
            }
            savedGamesMenu.RunMenu();
            return "";
        }

        private static string CreateNewGameOptions()
        {
            Console.WriteLine("Name your new Game settings");
            Console.Write(">");
            var name = Console.ReadLine() ?? "custom game option";
            var height = GetBoardSizeInputFromUser("WIDTH", C.MinBoardWidth, C.MaxBoardWidth);
            var width = GetBoardSizeInputFromUser("HEIGHT", C.MinBoardHeight, C.MaxBoardHeight);
            Console.WriteLine("Can same player move after hitting a boat? (Y/N)");
            Console.Write(">");
            var canSamePlayerMoveAfterHit = Console.ReadLine()?.Trim().ToLower().Equals("y") ?? true;
            Console.WriteLine("How ships should be placed?");
            Console.WriteLine("Ships CAN touch (Y) | Corners CAN touch (C) | Ships CAN NOT touch (N)");
            Console.Write(">");
            var input = Console.ReadLine()?.Trim().ToUpper();
            var boatsCanTouch = EBoatsCanTouch.No;
            switch (input)
            {
                case "Y":
                    boatsCanTouch = EBoatsCanTouch.Yes;
                    break;
                case "C":
                    boatsCanTouch = EBoatsCanTouch.Corner;
                    break;
                case "N":
                    boatsCanTouch = EBoatsCanTouch.No;
                    break;
            }
            var newGameOption = new GameOption
            {
                Name = name,
                BoardHeight = height,
                BoardWidth = width,
                CanSamePlayerMoveAfterHit = canSamePlayerMoveAfterHit,
                EBoatsCanTouch = boatsCanTouch
            };
            _currentGameOptionsId = _gameController.SaveGameOption(newGameOption);
            return "";
        }

        private static string ShowGameOptions()
        {
            var gameSettings = new Menu(MenuLevel.Level1);
            gameSettings.AddMenuItem(new MenuItem("Restore to default", "D", RestoreSettings));
            var gameOptionsList = _gameOptionsHandler.GetAllGameOptions();
            for (var i = 0; i < gameOptionsList.Count; i++)
            {
                var gameOption = gameOptionsList[i];
                gameSettings.AddMenuItem(new MenuItem(
                    $"Set '{gameOption.Name}' settings",
                    i.ToString(),
                    SetGameOptionsById(gameOption.GameOptionId)));
            }

            gameSettings.AddMenuItem(new MenuItem("Add new game option", "N", CreateNewGameOptions));
            gameSettings.RunMenu();
            return "";
        }

        private static Func<string> SetGameOptionsById(int gameOptionId)
        {
            _currentGameOptionsId = gameOptionId;
            return () => "m";
        }

        private static int GetBoardSizeInputFromUser(string paramName, int minValue, int maxValue)
        {
            do
            {
                Console.WriteLine($"What will be the game board {paramName}?");
                var input = Console.ReadLine()?.Trim();
                if (int.TryParse(input, out var result))
                {
                    if (ValidateInput(result, minValue, maxValue))
                    {
                        return result;
                    }
                }
                Console.WriteLine($"Game board {paramName} must be between {minValue} - {maxValue}");
            } while (true);
        }

        private static bool ValidateInput(int input, int minValue, int maxValue)
        {
            return input >= minValue && input <= maxValue;
        }
        

        private static string RestoreSettings()
        {
            var gameOption = new GameOption
            {
                Name = "default game option",
                BoardHeight = C.DefaultGameOptionBoardHeight,
                BoardWidth = C.DefaultGameOptionBoardWidth,
                CanSamePlayerMoveAfterHit = C.DefaultGameOptionCanSamePlayerMoveAfterHit,
                EBoatsCanTouch = C.DefaultGameOptionBoatsCanTouch
            };
            _currentGameOptionsId = _gameController.SaveGameOption(gameOption);
            return "r";
        }
        
        
        static void DrawBoards(Player? player)
        {
            var (playerBoard, opponentBoard) = _gameController.GetBoards();
            if (player == null || player.GameA != null)
            {
                BattleShipsConsoleUi.DrawBoards(playerBoard, opponentBoard);
            }
            else
            {
                BattleShipsConsoleUi.DrawBoards(opponentBoard, playerBoard);
            }
        }

        static void SelectGameOptionBoat(int y, int x, bool isVertical, Player player)
        {
            var gameDataHandler = new GameDataHandler(_appDbContext);
            var gameOptionBoats = gameDataHandler.GetGameOptionBoats(_currentGameOptionsId).Result;
            var gameOptionBoatsLeft = gameOptionBoats.Where(gob =>
                player.GameBoats.Count(gb => gb.Name == gob.Boat.Name && gb.Size == gob.Boat.Size) != gob.Quantity);
            int gameOptionBoatId;
            bool parseSuccessful;
            do
            {
                Console.WriteLine("Choose boat for player " + player.Name + ":");
                foreach (var gameOptionBoat in gameOptionBoatsLeft)
                {
                    Console.WriteLine(
                        $"{gameOptionBoat.GameOptionBoatId}) {gameOptionBoat.Boat.Name} size: {gameOptionBoat.Boat.Size}");
                }
                parseSuccessful = int.TryParse(Console.ReadLine()?.Trim(), out gameOptionBoatId);
            } while (!(parseSuccessful && gameOptionBoatsLeft.Any(gob => gob.GameOptionBoatId == gameOptionBoatId)));
            _gameController.PlaceShip(gameOptionBoatId, y, x, isVertical);
        }

        static Tuple<int, int> GetCoordinatesInputFromUser(int height, int width)
        {
            var x = -1;
            var y = -1;
            var conversionSuccessful = false;
            do
            {
                Console.WriteLine("Give coordinates Y (row), X (col)");
                var input = Console.ReadLine()?.Trim().ToLower();
                if (input == "exit")
                {
                    return new Tuple<int, int>(-2, -2);
                }
                var xy = input?.Split(",");
                if (xy != null && xy.Length == 2 && (!input?.StartsWith(",") ?? false) && (!input?.EndsWith(",") ?? false))
                {
                    conversionSuccessful = int.TryParse(xy[1], out x);
                    y = xy[0].ToCharArray()[0] - 'a';    
                }
            } while (!(y <= height && y >= 0 && x <= width && x >= 0 && conversionSuccessful));
            return new Tuple<int, int>(y, x - 1);
        }

        static void ShipPlacement(Player boardOwner, int height, int width)
        {
            string? input;
            var placementOptionCommandKeys = new List<string?>() {"r", "v", "h"};
            do
            {
                DrawBoards(boardOwner);
                Console.WriteLine("Choose a ship placement option");
                Console.WriteLine("'r' - random");
                Console.WriteLine("'v' - vertical");
                Console.WriteLine("'h' - horizontal");
                input = Console.ReadLine()?.Trim().ToLower();
            } while (!placementOptionCommandKeys.Contains(input));
            Tuple<int, int> coordinates;
            switch (input)
            {
                case "r" :
                    _gameController.PlaceShipsRandomly(_currentGameOptionsId);
                    break;
                case "v" :
                    coordinates = GetCoordinatesInputFromUser(height, width);
                    Console.WriteLine("coords" + coordinates);
                    SelectGameOptionBoat(coordinates.Item1, coordinates.Item2, true, boardOwner);
                    break;
                case "h" :
                    coordinates = GetCoordinatesInputFromUser(height, width);
                    Console.WriteLine("coords" + coordinates);
                    SelectGameOptionBoat(coordinates.Item1, coordinates.Item2, false, boardOwner);
                    break;
            }
            /*var shipPlacementMenu = new Menu(MenuLevel.Level0);
            
            shipPlacementMenu.AddMenuItem(new MenuItem("Place ships randomly", "1", () =>
            {
                _gameController.PlaceShipsRandomly(_currentGameOptionsId);
                return "";
            }));
            shipPlacementMenu.AddMenuItem(new MenuItem("Vertically place", "2", () =>
            {
                Console.WriteLine("Give starting coordinates X, Y (↓)");
                var input = Console.ReadLine();
                var xy = input?.Split(",");
                SelectGameOptionBoat(int.Parse(xy[0]), int.Parse(xy[1]), true, boardOwner);
                return "";
            }));
            shipPlacementMenu.AddMenuItem(new MenuItem("Horizontally place", "3",  () =>
            {
                Console.WriteLine("Give starting coordinates X, Y (→)");
                var input = Console.ReadLine();
                var xy = input?.Split(",");
                SelectGameOptionBoat(int.Parse(xy[0]), int.Parse(xy[1]), true, boardOwner);
                return "";
            }));
            shipPlacementMenu.RunMenu();*/
        }

        static void HandleShipPlacement(Game game)
        {
            var gameDataHandler = new GameDataHandler(_appDbContext);
            var gameOptionBoatsCount = (gameDataHandler.GetGameOptionBoats(_currentGameOptionsId)).Result.Sum(gob => gob.Quantity);
            while (game.PlayerA.GameBoats.Count + game.PlayerB.GameBoats.Count != gameOptionBoatsCount * 2 && _currentGameOptionsId != 0)
            {
                var boardOwner = game.PlayerA.GameBoats.Count == gameOptionBoatsCount ? game.PlayerB : game.PlayerA;
                ShipPlacement(boardOwner, game.BoardHeight, game.BoardWidth);
            }
        }

        static string PlayBattleShip(PlayersInfo? playersInfo)
        {
            int gameId;
            if (playersInfo != null)
            {
                if (_currentGameOptionsId == 0) RestoreSettings();
                gameId = _gameController.CreateNewGame(_currentGameOptionsId, playersInfo);
            }
            else
            {
                gameId = _gameController.LoadGame(_gameToLoadId).GameId;
            }

            var gameDataHandler = new GameDataHandler(_appDbContext);
            var game = gameDataHandler.GetGame(gameId).Result;
            HandleShipPlacement(game);
            Tuple<int, int> userInput;
            do
            {
                DrawBoards(null);
                Console.WriteLine("'exit' - to exit game");
                userInput = GetCoordinatesInputFromUser(game.BoardHeight, game.BoardWidth);
                if (userInput.Item1 != -2 && userInput.Item2 != -2)
                {
                    var (playerBoard, opponentBoard) = _gameController.PlayerSelected($"{userInput.Item1},{userInput.Item2}");
                    BattleShipsConsoleUi.DrawBoards(playerBoard, opponentBoard);
                }
            } while (userInput.Item1 != -2 && userInput.Item2 != -2);
            return "";
        }
        
        
        
        static string StartNewBattleShip()
        {
            var playersInfoMenu = new Menu(MenuLevel.Level1);
            playersInfoMenu.AddMenuItem(new MenuItem("Player vs. Player", "1", () =>
            {
                _playersInfo.PlayerAType = EPlayerType.Human;
                _playersInfo.PlayerBType = EPlayerType.Human;
                return "";
            }));
            playersInfoMenu.AddMenuItem(new MenuItem("Player vs. AI", "2", () =>
            {
                _playersInfo.PlayerAType = EPlayerType.Human;
                _playersInfo.PlayerBType = EPlayerType.AI;
                return "";
            }));
            playersInfoMenu.AddMenuItem(new MenuItem("AI vs. AI", "3", () =>
            {
                _playersInfo.PlayerAType = EPlayerType.AI;
                _playersInfo.PlayerBType = EPlayerType.AI;
                return "";
            }));
            do
            {
                Console.Write("Enter Player A name: ");
                _playersInfo.PlayerAName = Console.ReadLine() ?? "playerA";
            } while (string.IsNullOrWhiteSpace(_playersInfo.PlayerAName));
            do
            {
                Console.Write("Enter Player B name: ");
                _playersInfo.PlayerBName = Console.ReadLine() ?? "playerB";
            } while (string.IsNullOrWhiteSpace(_playersInfo.PlayerBName));
            return PlayBattleShip(_playersInfo);
        }
    }
}