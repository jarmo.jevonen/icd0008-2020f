# EXAM01
Exam took 16 hours to complete
---
***Task description***

Monitoring car expenses - WebApp

You have a client who is a car park manager for a big company. His job is to monitor all cars, maintain them and decide which cars are too costly to maintain.

Multiple cars, expenses as free text, Every expense also has a category  (fuel, tires, maintenance, insurance, etc), cost and date/time. Frontpage should display an overview of the cars, with their expense overview. Allow filtering based on free text search and categories (recalculate overall expenses based on visible data).

WebApp should allow:

Car CRUD

Categories CRUD

Car expenses - CRUD, search/filtering, reporting


Generally speaking - these are only broad guidelines. Please write a solution, that you would like to present to the world as your best effort in programming and app-designing (UX is the key).

To be implemented as Razor Pages based Web app. No ViewBags! Nullable Reference Types have to be enabled solution wide.

---
***Database commands***
~~~~
dotnet ef migrations add InitialMigration --project DAL --startup-project WebApp
dotnet ef database update  --project DAL --startup-project WebApp
dotnet ef database drop  --project DAL --startup-project WebApp
~~~~
***Scaffold pages***
~~~~
cd WebApp
dotnet aspnet-codegenerator razorpage -m DomainClass -dc AppDbContext -udl -outDir Pages/DomainClass --referenceScriptLibraries -f
~~~~
