﻿using System.Collections.Generic;
using Domain;

namespace WebApp.DTO
{
    public class CarInfoDTO
    {
        public Car Car { get; set; } = default!;
        public ICollection<Expense> LatestExpenses { get; set; } = default!;
        public decimal CarExpensesCost { get; set; }
    }
}