﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.DTO;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private const int LastExpensesCount = 3;

        private readonly AppDbContext _context;

        public ICollection<CarInfoDTO> CarsInfoDtos { get; set; } = default!;

        [BindProperty(SupportsGet = true)] public string? Keyword { get; set; }
        [BindProperty(SupportsGet = true)] public int? ExpenseCategoryId { get; set; }
        [BindProperty(SupportsGet = true)] public DateTime? Start { get; set; }
        [BindProperty(SupportsGet = true)] public DateTime? End { get; set; }


        public SelectList ExpenseCategoriesSelectList { get; set; } = default!;


        public IndexModel(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var carQuery = _context.Cars
                .Include(c => c.Expenses)
                    .ThenInclude(e => e.ExpenseCategory)
                .Include(c => c.Model!)
                    .ThenInclude(m => m.Brand)
                .Select(c => new CarInfoDTO()
                {
                    Car = c,
                    CarExpensesCost = 0,
                    LatestExpenses = new List<Expense>()
                })
                .AsQueryable();

            var expensesQuery = _context.Expenses
                .Include(e => e.ExpenseCategory)
                .AsQueryable();
                Keyword = Keyword?.Trim();
            if (!string.IsNullOrWhiteSpace(Keyword))
            {
                carQuery = carQuery.Where(cid => cid.Car.RegistrationNumber.Contains(Keyword)
                                                 || cid.Car.Model!.Name.Contains(Keyword)
                                                 || cid.Car.ManufactureYear.ToString().Equals(Keyword)
                                                 || cid.Car.Model.Brand!.Name.Contains(Keyword)
                                                 || cid.Car.Expenses!
                                                     .Any(e => e.Description.Contains(Keyword)
                                                               || e.Name.Contains(Keyword)));
                expensesQuery = expensesQuery.Where(e => e.Name.Contains(Keyword)
                                                         || e.Description.Contains(Keyword));
            }

            if (Start.HasValue)
            {
                carQuery = carQuery.Where(cid => cid.Car.Expenses!.Any(e => e.Time >= Start));
                expensesQuery = expensesQuery.Where(e => e.Time >= Start);
            }

            if (End.HasValue)
            {
                carQuery = carQuery.Where(cid => cid.Car.Expenses!.Any(e => e.Time <= End));
                expensesQuery = expensesQuery.Where(e => e.Time <= End);
            }

            if (ExpenseCategoryId != null && ExpenseCategoryId != 0)
            {
                carQuery = carQuery.Where(cid => cid.Car.Expenses!.Any(e => e.ExpenseCategoryId == ExpenseCategoryId));
                expensesQuery = expensesQuery.Where(e => e.ExpenseCategoryId == ExpenseCategoryId);
            }

            var expenses = await expensesQuery.ToListAsync();
            CarsInfoDtos = await carQuery.ToListAsync();

            foreach (var carInfoDto in CarsInfoDtos)
            {
                carInfoDto.LatestExpenses = expenses?
                    .Where(e => e.CarId == carInfoDto.Car.CarId)
                    .OrderByDescending(e => e.Time)
                    .Take(LastExpensesCount).ToList()!;
                
                carInfoDto.CarExpensesCost = expenses?
                    .Where(e => e.CarId == carInfoDto.Car.CarId)
                    .Sum(e => e.Cost) ?? 0;
            }
            

            ExpenseCategoriesSelectList = new SelectList(
                await _context.ExpenseCategories.ToListAsync(),
                nameof(ExpenseCategory.ExpenseCategoryId),
                nameof(ExpenseCategory.Name)
            );

            return Page();
        }
    }
}