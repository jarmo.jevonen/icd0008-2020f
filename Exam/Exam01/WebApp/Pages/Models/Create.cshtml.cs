using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages_Models
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList BrandSelectList { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync()
        {
            BrandSelectList = new SelectList(
                await _context.Brands.ToListAsync(),
                nameof(Brand.BrandId),
                nameof(Brand.Name));
            return Page();
        }

        [BindProperty] public Model Model { get; set; } = default!;

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                BrandSelectList = new SelectList(
                    await _context.Brands.ToListAsync(),
                    nameof(Brand.BrandId),
                    nameof(Brand.Name),
                    nameof(Model.BrandId)
                    );
                return Page();
            }

            Model.BrandName = (await _context.Brands.FirstOrDefaultAsync(b => b.BrandId == Model.BrandId)).Name;

            _context.Models.Add(Model);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}