using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Models
{
    public class EditModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public EditModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList BrandSelectList { get; set; } = default!;

        [BindProperty]
        public Model Model { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Model = await _context.Models
                .Include(m => m.Brand).FirstOrDefaultAsync(m => m.ModelId == id);

            if (Model == null)
            {
                return NotFound();
            }
            
            BrandSelectList = new SelectList(
                await _context.Brands.ToListAsync(),
                nameof(Brand.BrandId),
                nameof(Brand.Name));
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                BrandSelectList = new SelectList(
                    await _context.Brands.ToListAsync(),
                    nameof(Brand.BrandId),
                    nameof(Brand.Name),
                    nameof(Model.BrandId)
                );
                return Page();
            }

            var oldBrandId = (await _context.Models.AsNoTracking().FirstOrDefaultAsync(m => m.ModelId == Model.ModelId)).BrandId;

            var newBrand = await _context.Brands.FirstOrDefaultAsync(b => b.BrandId == Model.BrandId);
            
            
            if (Model.BrandId != oldBrandId)
            {
                Model.BrandName = newBrand.Name;
            }
            
            _context.Attach(Model).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModelExists(Model.ModelId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ModelExists(int id)
        {
            return _context.Models.Any(e => e.ModelId == id);
        }
    }
}
