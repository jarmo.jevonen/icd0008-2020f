using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Expenses
{
    public class EditModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public EditModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList ExpenseCategorySelectList { get; set; } = default!;

        [BindProperty]
        public Expense Expense { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Expense = await _context.Expenses
                .Include(e => e.Car)
                .Include(e => e.ExpenseCategory).FirstOrDefaultAsync(m => m.ExpenseId == id);

            if (Expense == null)
            {
                return NotFound();
            }
            
            ExpenseCategorySelectList = new SelectList(
                await _context.ExpenseCategories.ToListAsync(),
                nameof(ExpenseCategory.ExpenseCategoryId),
                nameof(ExpenseCategory.Name));
            
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ExpenseCategorySelectList = new SelectList(
                    await _context.ExpenseCategories.ToListAsync(),
                    nameof(ExpenseCategory.ExpenseCategoryId),
                    nameof(ExpenseCategory.Name),
                    nameof(Expense.ExpenseCategoryId)
                    );
                return Page();
            }

            Expense.CarId = (await _context.Expenses.AsNoTracking().FirstOrDefaultAsync(e => e.ExpenseId == Expense.ExpenseId)).CarId;
            _context.Attach(Expense).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExpenseExists(Expense.ExpenseId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ExpenseExists(int id)
        {
            return _context.Expenses.Any(e => e.ExpenseId == id);
        }
    }
}
