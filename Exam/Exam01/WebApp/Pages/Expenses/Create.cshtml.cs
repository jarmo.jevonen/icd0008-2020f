using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages_Expenses
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public SelectList ExpenseCategorySelectList { get; set; } = default!;

        public string CarInfo { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync()
        {
            if (CarId == null)
            {
                return NotFound();
            }
            
            var car = await _context.Cars
                .Include(c => c.Model!)
                .ThenInclude(m => m.Brand)
                .FirstOrDefaultAsync(c => c.CarId == CarId);
            
            if (car == null)
            {
                return NotFound();
            }
            
            CarInfo = car.CarInfo;
            
            ExpenseCategorySelectList = new SelectList(
                await _context.ExpenseCategories.ToListAsync(),
                nameof(ExpenseCategory.ExpenseCategoryId),
                nameof(ExpenseCategory.Name)
            );
            
            return Page();
        }

        [BindProperty(SupportsGet = true)] public int? CarId { get; set; }

        [BindProperty] public Expense Expense { get; set; } = default!;

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ExpenseCategorySelectList = new SelectList(
                    await _context.ExpenseCategories.ToListAsync(),
                    nameof(ExpenseCategory.ExpenseCategoryId),
                    nameof(ExpenseCategory.Name),
                    nameof(Expense.ExpenseCategoryId)
                );
                return Page();
            }

            _context.Expenses.Add(Expense);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Index");
        }
    }
}