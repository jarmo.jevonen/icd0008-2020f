using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages_Cars
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }


        public SelectList ModelSelectList { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync()
        {
            var models = await _context.Models
                .Include(m => m.Brand)
                .ToListAsync();
            
            ModelSelectList = new SelectList(
                models,
                nameof(Model.ModelId),
                nameof(Model.Name),
                null,
                nameof(Model.BrandName)
            );
        
            return Page();
        }

        [BindProperty]
        public Car Car { get; set; } = default!;

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ModelSelectList = new SelectList(
                    await _context.Models.ToListAsync(),
                    nameof(Model.ModelId),
                    nameof(Model.Name),
                    nameof(Car.ModelId),
                    nameof(Model.BrandName)
                );
                return Page();
            }

            _context.Cars.Add(Car);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
