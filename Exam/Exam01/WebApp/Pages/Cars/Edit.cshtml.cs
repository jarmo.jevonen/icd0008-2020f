using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Cars
{
    public class EditModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public EditModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Car Car { get; set; } = default!;

        public string CarInfo { get; set; } = default!;

        public SelectList ModelSelectList { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            CarInfo = (await _context.Cars
                .Include(c => c.Model!)
                .ThenInclude(m => m.Brand)
                .FirstOrDefaultAsync(c => c.CarId == id)).CarInfo;
            
            if (id == null)
            {
                return NotFound();
            }

            Car = await _context.Cars
                .Include(c => c.Model).FirstOrDefaultAsync(m => m.CarId == id);

            if (Car == null)
            {
                return NotFound();
            }

            var models = await _context.Models
                .Include(m => m.Brand)
                .ToListAsync();
            
            ModelSelectList = new SelectList(
                models,
                nameof(Model.ModelId),
                nameof(Model.Name),
                null,
                nameof(Model.BrandName)
            );
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                var models = await _context.Models
                    .Include(m => m.Brand)
                    .ToListAsync();
            
                ModelSelectList = new SelectList(
                    models,
                    nameof(Model.ModelId),
                    nameof(Model.Name),
                    nameof(Car.ModelId),
                    nameof(Model.BrandName)
                );
                CarInfo = (await _context.Cars
                    .Include(c => c.Model!)
                    .ThenInclude(m => m.Brand)
                    .FirstOrDefaultAsync(c => c.CarId == Car.CarId)).CarInfo;
                return Page();
            }

            _context.Attach(Car).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarExists(Car.CarId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CarExists(int id)
        {
            return _context.Cars.Any(e => e.CarId == id);
        }
    }
}
