﻿using System;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : DbContext
    {

        public DbSet<Brand> Brands { get; set; } = default!;
        public DbSet<Car> Cars { get; set; } = default!;
        public DbSet<Expense> Expenses { get; set; } = default!;
        public DbSet<ExpenseCategory> ExpenseCategories { get; set; } = default!;
        public DbSet<Model> Models { get; set; } = default!;

        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }
    }
}