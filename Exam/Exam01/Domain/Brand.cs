﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Brand
    {
        public int BrandId { get; set; }
        
        [DisplayName("Brand")]
        [MinLength(1)]
        [MaxLength(25)]
        public string Name { get; set; } = default!;

        [DisplayName("Image URL")]
        [MaxLength(200)]
        public string? ImageUrl { get; set; }
        
        public ICollection<Model>? Models { get; set; }
    }
}