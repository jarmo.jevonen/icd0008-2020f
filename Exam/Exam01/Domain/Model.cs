﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Domain
{
    public class Model
    {
        public int ModelId { get; set; }
        
        [DisplayName("Brand")]
        public int BrandId { get; set; }
        public Brand? Brand { get; set; }

        public string? BrandName { get; set; } = default!;
        
        [DisplayName("Model")]
        public string Name { get; set; } = default!;
        
        public ICollection<Car>? Cars { get; set; }
    }
}