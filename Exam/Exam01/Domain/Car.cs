﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Car
    {
        public int CarId { get; set; }
        
        [DisplayName("Model")]
        public int ModelId { get; set; }
        public Model? Model { get; set; }
        
        [DisplayName("Reg. number")]
        [MaxLength(25)]
        [MinLength(1)]
        public string RegistrationNumber { get; set; } = default!;
        
        [DisplayName("Year of manufacture")]
        [Range(1000, 9999)]
        public int ManufactureYear { get; set; }
        
        public ICollection<Expense>? Expenses { get; set; }

        public string CarInfo => $"{Model?.Brand?.Name} {Model?.Name} ({ManufactureYear})";
    }
}