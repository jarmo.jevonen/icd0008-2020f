﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DisplayName("Expense category")]
    public class ExpenseCategory
    {
        public int ExpenseCategoryId { get; set; }
        
        [MinLength(1)]
        [MaxLength(50)]
        [DisplayName("Category")]
        public string Name { get; set; } = default!;
        
        public ICollection<Expense>? Expenses { get; set; }
    }
}