﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Expense
    {
        public int ExpenseId { get; set; }

        [MinLength(1)]
        [MaxLength(25)]
        [DisplayName("Name")]
        public string Name { get; set; } = default!;
        
        
        [Range(0, int.MaxValue)]
        [DisplayName("Cost")]
        public int Cost { get; set; }
        
        [MinLength(1)]
        [MaxLength(500)]
        [DisplayName("Description")]
        public string Description { get; set; } = default!;
        
        [DisplayName("Car")]
        public int CarId { get; set; }
        public Car? Car { get; set; }
        
        
        [DisplayName("Category")]
        public int ExpenseCategoryId { get; set; }
        public ExpenseCategory? ExpenseCategory { get; set; }
        
        
        [DisplayName("Time")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime Time { get; set; }

        public string LastExpenseInfo => $"{Name} - {Cost}€";

        public string DescriptionPreview => Description.Length <= 50 ? Description : Description.Substring(0, 49) + "...";

    }
}